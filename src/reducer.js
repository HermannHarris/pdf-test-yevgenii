const initialState = {
  headers: ["City", "Temperature (C)", "Humidity (%)", "Pressure (mm Hg)", "Cloudiness (%)", "Wind (m/s)"],
  data: []
}

export default function weatherApp(state = initialState, action) {
  switch (action.type) {
    case 'FETCHING_DATA_FINISHED':
      return {
        ...state,
        data: [...state.data, action.weatherData]
      }
    default:
      return state;
  }
}