export const fetchData = (lat, lng) => (dispatch) => {
  const endpoint = `http://api.openweathermap.org/data/2.5/weather?APPID=7f6619394d62483e856ea81df3979f97&units=metric&lat=${lat}&lon=${lng}`;
  fetch(endpoint)
    .then(response => response.json())
    .then(data => {
      const weatherData = [
        data.name,
        Math.round(data.main.temp),
        data.main.humidity,
        data.clouds.all,
        Math.round(data.wind.speed),
        Math.round(data.main.pressure * 0.75)
      ]
      return dispatch({
        type: 'FETCHING_DATA_FINISHED',
        weatherData
      });
    });
}