import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Button } from 'semantic-ui-react';
import jsPDF from 'jspdf';
import 'jspdf-autotable';

import { fetchData } from './actions';
import coordsData from './coordsData.json';
import { getUniqueId } from './utils';

class App extends Component {
  componentDidMount() {
    Object.values(coordsData).forEach((item) => {
      const lat = item.split(', ')[0];
      const lng = item.split(', ')[1];
      this.props.fetchData(lat, lng);
    });
  }

  renderHeaders = () => {
    return (
      <Table.Row>
        {this.props.headers.map((header) => <Table.HeaderCell key={getUniqueId()}>{header}</Table.HeaderCell>)}
      </Table.Row>
    )
  }

  renderRow = (item) => {
    return (
      <Table.Row key={getUniqueId()}>
        {item.map((prop) => <Table.Cell key={getUniqueId()}>{prop}</Table.Cell>)}
      </Table.Row>
    )
  }

  handleClick = () => {
    const doc = new jsPDF('landscape', 'pt');
    doc.setProperties({
      title: "Weather.pdf"
    });
    doc.autoTable(this.props.headers, this.props.data);

    const file = doc.output('blob');
    const reader = new FileReader();
    reader.onload = () => {
      window.open(reader.result)
    };
    reader.readAsDataURL(file);
  }

  render() {
    return (
      <div>
        <Table celled striped>
          <Table.Header>
            {this.renderHeaders()}
          </Table.Header>

          <Table.Body>
            {this.props.data.map((item) => this.renderRow(item))}
          </Table.Body>
        </Table>

        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Button basic color='red' onClick={this.handleClick}>Open in PDF</Button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    headers: state.headers,
    data: state.data
  };
};

export default connect(mapStateToProps, { fetchData })(App);